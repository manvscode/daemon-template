#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <syslog.h>
#include "daemon.h"

#define DAEMON_LOCKFILE_PERM       (S_IRUSR | S_IWUSR)


int daemon_start( const char* lockfile, const daemon_signals_t* signals )
{
	int result = 0;
	pid_t p = fork( );

	if( p < 0 )
	{
		perror( "ERROR" );
		result = -1;
		goto done;
	}
	else if( p > 0 )
	{
		/* Kill off parent process */
		result = 1;
		goto done;
	}

	/* Create our own session */
	if( setsid( ) < 0 )
	{
		perror( "ERROR" );
		result = -1;
		goto done;
	}

	/* Change to the root directory */
	if( chdir( "/" ) < 0 )
	{
		perror( "ERROR" );
		result = -1;
		goto done;
	}

	#if 1
	/* u+rw */
	umask( S_IRUSR | S_IWUSR ); /* Should this be 0? */
	#else
	/* u+rw, g+r, o+r */
	umask( S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH );
	#endif

	int lfp = open( lockfile, O_WRONLY | O_CREAT | O_TRUNC, DAEMON_LOCKFILE_PERM );
	if( lfp < 0 )
	{
		perror( "ERROR" );
		result = -1;
		goto done;
	}
	else
	{
		if( lockf( lfp, F_TLOCK, 0 ) < 0 )
		{
			perror( "ERROR" );
			result = -1;
			goto done;
		}

		/* only first instance continues */
		char buffer[ 64 ];
		int len = snprintf( buffer, sizeof(buffer), "%d",  getpid( ) );

		/* record pid to lockfile */
		write( lfp, buffer, len );
	}


	if( close( STDIN_FILENO ) < 0 )
	{
		perror( "ERROR" );
		result = -1;
		goto done;
	}

	if( close( STDOUT_FILENO ) < 0 )
	{
		perror( "ERROR" );
		result = -1;
		goto done;
	}

	if( close( STDERR_FILENO ) < 0 )
	{
		perror( "ERROR" );
		result = -1;
		goto done;
	}

	if( signals )
	{
		if( signals->sigpipe_fxn ) signal( SIGPIPE, signals->sigpipe_fxn );
		if( signals->sigstop_fxn ) signal( SIGSTOP, signals->sigstop_fxn );
		if( signals->sighup_fxn )  signal( SIGHUP, signals->sighup_fxn );   /* catch hangup signal */
		if( signals->sigterm_fxn ) signal( SIGTERM, signals->sigterm_fxn ); /* catch kill signal */
	}

done:
	return result;
}

int daemon_restart( const char* lockfile )
{
	long lfp = open( lockfile, O_RDONLY, DAEMON_LOCKFILE_PERM );

	if( lfp < 0 )
	{
		#if 0
		/* File did not exist so start the daemon. */
		return daemon_start( lockfile, signals );
		#else
		return -1;
		#endif
	}

	char buffer[ 64 ]; // safe
	read( lfp, buffer, sizeof(buffer) );
	char *endptr = NULL;
    long daemon_pid = strtol( buffer, &endptr, 0 );

	if( daemon_pid <= 0 )
	{
		return -1;
	}

	return kill( daemon_pid, SIGHUP );
}

int daemon_stop( const char* lockfile )
{
	long lfp = open( lockfile, O_RDONLY, DAEMON_LOCKFILE_PERM );

	if( lfp < 0 )
	{
		/* File did not exist so daemon was never started. */
		return 0;
	}

	char buffer[ 64 ]; // safe
	read( lfp, buffer, sizeof(buffer) );
	char *endptr = NULL;
    long daemon_pid = strtol( buffer, &endptr, 0 );

	if( daemon_pid <= 0 )
	{
		return -1;
	}
	else
	{
		remove( lockfile );
	}

	return kill( daemon_pid, SIGTERM );
}

