#ifndef _DAEMON_H_
#define _DAEMON_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct daemon_signals {
	sig_t sigpipe_fxn; /* SIGPIPE */
	sig_t sigstop_fxn; /* SIGSTOP */
	sig_t sighup_fxn;  /* SIGHUP */
	sig_t sigterm_fxn; /* SIGTERM */
} daemon_signals_t;

int daemon_start   ( const char* lockfile, const daemon_signals_t* signals );
int daemon_restart ( const char* lockfile );
int daemon_stop    ( const char* lockfile );

#define daemon_log( p, ...)   syslog( p, __VA_ARGS__ );

#ifdef __cplusplus
} /* C linkage */
#endif
#endif /* _DAEMON_H_ */
