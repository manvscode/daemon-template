#include <stdio.h>
#include <stdbool.h>
#include "daemon.h"


bool service_start     ( bool daemon_mode );
void service_end       ( void );

int main( int argc, char* argv[] )
{

	return 0;
}

bool service_start( bool daemon_mode )
{
	if( daemon_mode )
	{
		if( daemon_start( "myservice" ) < 0 )
		{
			return false;
		}
	}

	/* TODO: Initialization goes here */
}

void service_end( void )
{
	/* TODO: Deinitialization goes here */

	daemon_end( );
}
